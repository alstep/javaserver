<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
  <title>Hello</title>
</head>
<body>
  <h3>Hello, world!!!</h3>
  <p>Date: ${date}</p>
  <c:forEach var="student" items="${students}">
    <p>Hello, <c:if test="${student.sex=='MALE'}">Mr.</c:if><c:if test="${student.sex=='FEMALE'}">Ms.</c:if> ${student.name}!</p>
  </c:forEach>
</body>
</html>


