package org.itschool_hillel.java.server;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@WebServlet(value="/helloStudents")
public class HelloStudentsServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("date", new SimpleDateFormat("dd.MM.yyyy").format(new Date()));

        List<Student> students = new ArrayList<>();
        students.add(new Student("John Smith", Sex.MALE));
        req.setAttribute("students", students);

        req.getRequestDispatcher("/WEB-INF/templates/hello.jsp").forward(req, resp);
    }
}
