package org.itschool_hillel.java.server;

public enum Sex {
    MALE, FEMALE
}
